import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';
import fetch from 'node-fetch';


declare var google: any;
declare var require: any

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})

export class AppComponent {
  title = 'test-app';
  currentPage = 'home';
  teacherFilter = 'Regular';

  teacher = {
      name: '',
      address: '',
      city: '',
      state: '',
      zip: '',
      status: ''
  };

  school = {
      name: '',
      address: '',
      city: '',
      state: '',
      zip: '',
      status: ''
  };

  schedule = {
    date: '',
    time: '',
    school: ''
  }

  teachers:any;
  subTeachers:any;
  schools: any;
  classes = [];

  regularTeachers = [];
  dropdownSettings = {};
  selectedItems = [];

  estimations = [];
  destination = {
    name: '',
    school: '',
    address: '',
    subject: '',
    arrival: new Date
  };
  displayList = false;
  currentArrival: any;
  currentSub = '';

  config = [];
  results = [];
  currConfig: any;
  schedules: any;
  //googleMapsClient: any;

  private logistics: any;

  constructor(private db: AngularFirestore, private http: HttpClient) {}

  ngOnInit(): void{

    this.getTeachers();

    this.getSchools().subscribe( result => {
      this.schools = result;
      this.setClasses(result);
    });

    this.dropdownSettings = {
          singleSelection: false,
          idField: 'item_id',
          textField: 'item_text',
          selectAllText: 'Select All',
          unSelectAllText: 'UnSelect All',
          itemsShowLimit: 105,
          allowSearchFilter: true,
          limitSelection: 105
        };

  }

  onItemSelect(item: any) {
  //  const time = item.split("(").pop().split(")").pop().split(":")[0];
    var address = this.schools[this.schedule.school].payload.doc.data().address + ', ' + this.schools[this.schedule.school].payload.doc.data().city + ', ' + this.schools[this.schedule.school].payload.doc.data().state + ', ' + this.schools[this.schedule.school].payload.doc.data().zip;
    console.log(item);
    var data = {school: this.schools[this.schedule.school].payload.doc.data().name, destination: address, date: this.schedule.date, class: item.item_text, time: item.item_id};
    this.config.push(Object.assign({}, data));
    this.config.sort(function(a: any,b: any) {
        return new Date(b.date).getTime() - new Date(a.date).getTime();
    });
  }

  updateSelectedClasses(): void {
    this.selectedItems = [];
    for(let item of this.config){
      if(item.date == this.schedule.date){
        this.selectedItems.push(item.class);
      }
    }
  }

  setClasses(data): void{
    var x = 1, y = 35;
    var classSchedulesText = ['8:00 AM', '9:00 AM', '10:00 AM', '11:00 AM', '1:00 PM', '2:00 PM', '3:00 PM'];
    var classSchedulesValue = ['08','09','10','11','13','14','15'];
    for(let school of data){
      var classesContainer = [];
      var w = 4, i = 0;
      for(var z = 0; z < y; z++){
        if(z > w){
          i++;
          w = w + 5;
        }
       classesContainer.push( { item_id: classSchedulesValue[i] + '-' + x , item_text: 'Class ' + x + ' (' + classSchedulesText[i] +')' });
       x++;
     }
     this.classes.push(classesContainer);
    }
    console.log(this.classes);
  }

  async getDistanceMatrix(data): Promise<any>{
    var origins = [];
    for(let substitute of this.subTeachers){
      const origin = substitute.payload.doc.data().address + '+' + substitute.payload.doc.data().city + '+' + substitute.payload.doc.data().state + '+' + substitute.payload.doc.data().zip;
      origins.push(origin);
    }
    //var data = {origins: origins};
    var originAddresses = this.encodeQueryData(origins);
    var url = 'https://maps.googleapis.com/maps/api/distancematrix/json?origins='+ originAddresses +'&destinations= ' + encodeURIComponent(data.address) + '&arrival_time='+ data.arrival.getTime()+'&key=AIzaSyD4uq0ij9VFP9T77YUZMv_tMP_xOGV0uqo';
    const promise = fetch('https://cors-anywhere.herokuapp.com/' + url);

    return promise
      .then(res => res.json())
      .then(data => {return data})
      .catch();
  }

  bestGuess(logistics){
    console.log(logistics);
      var value = 0;
      var best = {};
      var x = 0, index = 0;
      for(let  result of logistics.rows){
        this.currentSub = logistics.origin_addresses[x];
        if(value == 0) {
          value = result.elements[0].duration.value;
          best = result.elements[0];
          index = x;
        } else if (value > result.elements[0].duration.value){
            var hasClass = this.hasScheduledClass();
            console.log(hasClass);
            if(!hasClass){
              value = result.elements[0].duration.value;
              best = result.elements[0];
              index = x;
            }
        }
        x++;
      }
    this.results.push({origin: logistics.origin_addresses[index], logistics: best, config: this.currConfig} );
  }


  async getLogistics(): Promise<any>{
    this.results = [];
    for(let item of this.config){
        var data = {
          school: item.school,
          class: item.class,
          address: item.destination,
          arrival: new Date(item.date + ' '+ item.time.split('-')[0] + ':00:00')
        }

        this.currConfig = data;

        const distance = await this.getDistanceMatrix(data);
        this.bestGuess(distance);
    }

    this.createSchedule();

  }


  createSchedule(){
    this.schedules = [];
    for(let substitute of this.subTeachers){
      const origin = substitute.payload.doc.data().address + ', ' + substitute.payload.doc.data().city + ', ' + substitute.payload.doc.data().state + ', ' + substitute.payload.doc.data().zip;
      var classes = [];
      for(let result of this.results){
        console.log(origin + ' == ' + result.origin);
        console.log(this.similar(origin, result.origin));
        if(this.similar(origin, result.origin) > 30){
          classes.push(result);
        }
      }
        this.schedules.push({name: substitute.payload.doc.data().name, classes: classes});

    }
    console.log(this.schedules);
  }


encodeQueryData(data) {
   const ret = [];
   for (let d in data)
     ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
   return ret.join('|');
}

  hasScheduledClass(){
    for(let result of this.results){
      if(this.currConfig.arrival.getTime() == result.config.arrival.getTime()){
        if(result.origin === this.currentSub){
          console.log(result.origin);
          console.log(this.currentSub);
          return true;
        }
      }
    }

    return false;
  }
  similar(a,b) {
      var lengthA = a.length;
      var lengthB = b.length;
      var equivalency = 0;
      var minLength = (a.length > b.length) ? b.length : a.length;
      var maxLength = (a.length < b.length) ? b.length : a.length;
      for(var i = 0; i < minLength; i++) {
          if(a[i] == b[i]) {
              equivalency++;
          }
      }


      var weight = equivalency / maxLength;
      return (weight * 100);
  }
  private getRandomSchool() {
    let randomIndex = Math.floor((Math.random() * this.schools.length) );
    return this.schools[randomIndex];
  }

  onSubmit(type): void {
    switch(type){
      case 'teachers':
        this.create(this.teacher, type);
        break;
      case 'schools':
        this.create(this.school, type);
        break;
    }

  }

  create(data, type){

    this.db.collection(type).add({
        name: data.name,
        address: data.address,
        state: data.state,
        city: data.city,
        zip: data.zip,
        status: data.status
    })

    this.teacher = {
        name: '',
        address: '',
        city: '',
        state: '',
        zip: '',
        status: ''
    };

    this.school = {
        name: '',
        address: '',
        city: '',
        state: '',
        zip: '',
        status: ''
    };
  }

  getTeachers(){
    this.db.collection('/teachers').snapshotChanges().subscribe( result => {
      this.teachers = result;
    })

    this.db.collection('/teachers', ref => ref.where('status', '==', 'Regular')).snapshotChanges().subscribe( result => {
      for(let teacher of result){
        this.regularTeachers.push({ item_id: teacher.payload.doc.id, item_text: teacher.payload.doc.data()['name'] });
      }
    })

    this.db.collection('/teachers', ref => ref.where('status', '==', 'Substitute')).snapshotChanges().subscribe( result => {
      this.subTeachers = result;
    })
  }

  getSchools(){
    return this.db.collection('/schools').snapshotChanges();
  }

  delete(key, type){
    Swal.fire({
      text: 'Are you sure you want to delete this teacher?',
      type: 'error',
      showCancelButton: true,
      confirmButtonText: 'Yes, please!'
    }).then((result) => {
      if(result.value){
        switch(type){
          case 'teachers':
            this.db.collection('teachers').doc(key).delete()
            break;
          case 'schools':
            this.db.collection('schools').doc(key).delete()
            break;
        }

      }
    })
  }

}
