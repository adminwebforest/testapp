// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDRMJW-DGeVHH4s2r60TZXIs5MrpA6kPxk",
    authDomain: "webbmason-a14ab.firebaseapp.com",
    databaseURL: "https://webbmason-a14ab.firebaseio.com",
    projectId: "webbmason-a14ab",
    storageBucket: "webbmason-a14ab.appspot.com",
    messagingSenderId: "1039444308148",
    appId: "1:1039444308148:web:bf89cb4aee0f045a"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
